# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This project is a Generic LinkedList implementation with plenty of utilities written in C.
the idea came over from the pdf included in the repo named LinkedListProblems.pdf. And each of the functions implemented is an exercise from the book. I simply made the implementation Generic aka every function takes a void pointer in some data type. Important: For the majority of function to work, List must contain one type of data in each instance and also custom compare methods must be provided.

* What does it do exactly?
Read LinkedListProblems.pdf and comments in LinkedList.h in order to take an idea of what each function does.

### How do I get set up? ###

* Summary of set up
Type 'make all' and you have a ready executable named LinkedList

* Dependencies:
gcc compiler and make tool

* How to run tests? -
Simply run LinkedList and watch the comments in main.c file

### Who do I talk to? ###
Repo owner: lyzadmortem@gmail.com