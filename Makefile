LinkedList.o: LinkedList.c LinkedList.h
	gcc -c LinkedList.c

main.o: main.c
	gcc -c main.c

LinkedList: LinkedList.o main.o
	gcc LinkedList.o main.o -o LinkedList 

all: LinkedList

clean:
	rm *.o LinkedList
