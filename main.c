/* 
 * File:   main.c
 * Author: Dimitris Lygizos
 *
 * Created on October 4, 2014, 9:14 PM
 */

#include "LinkedList.h"
#include <string.h>

/* function which prints
 *the contents of the list
 */
void printfunction(char *val) {
	printf("%s\n", val);
}

// custom comparison function
int intCompare(int *a, int *b)
{
	if(*a > *b) return 1;
	else if( *a == *b ) return 0;
	else return -1;
}

// Push a char in the head of the list
void p(node **list, char *x)
{
	char *t = x;
    Push( list, t );

}
/*
 * 
 */
int main(int argc, char** argv) {
	node *newNode, *front, *back, *list_c, *myList = NULL, *list_b = NULL;
    int search, index;
    char *anElement, *x;

    // Empty List Testing
    printf("Length of myList is %d\n", Length(myList));		// should print 0
    Pop(&myList);											// should throw an emptylist error

    index = 4;
    GetNth(list_b, index);									// should throw an invalid parameter error
    printContents(myList, printfunction);					// shall print nothing
    anElement = "Dimitris Lygizos";
    printf("Item %d appears %d times in list\n", *anElement, Count(myList, anElement, strcmp));	// shall print 0 times
    RemoveDuplicates(list_b, strcmp);
    printf("%s\n", "========================");
    /* Normal push test */

    x = "Test";				// Push some strings into the list
    p(&myList, x);
    x = "Second_element";
    p(&myList, x);
    x = "Third_element";
    p(&myList, x );
    x = "forth_element";
    p( &myList, x );
    x = "Last_element";
    p( &myList, x );
    /*Push( &myList, 14 );
    Push( &myList, 0 );
    Push( &myList, 7 );
    Push( &myList, 3 );*/

    /* Shall print
     * Last_element
     * forth_element
     * third_element
     * Second_element
     * Test
     */
    printContents(myList, printfunction);
    printf("%s\n", "========================");
    // Move Nodes in lists test
    MoveNode(&list_b, &myList);				// Move TOP from myList to list_b (list_b is null)
    printf("Mylist\n");

    /* Shall Print
     * forth_element
     * third_element
     * Second_element
     * Test
     */
	printContents(myList, printfunction);
	printf("%s\n", "========================");
	/* Shall print
	 * Last_element
	 */
	printf("list_b\n");
	printContents(list_b, printfunction);
	printf("%s\n", "========================");

	/* Push Test */
    x = "28";
    p( &list_b, x );
    x = "13";
    p( &list_b, x );
    x = "efta";
    p( &list_b, x );

    /* Shall Print
     * forth_element
     * third_element
     * Second_element
     * Test
     */
    printf("Mylist\n");
	printContents(myList, printfunction);
	printf("%s\n", "========================");

	/* Shall Print
     * efta
     * 13
     * 28
     * Last_element
     */
	printf("list_b\n");
	printContents(list_b, printfunction);
	printf("%s\n", "========================");

    // insert at index 4 on myList
	index = 4;
    InsertNth(&myList, index, anElement);

    /* List shall now contains
     * forth_element
     * third_element
     * Second_element
     * Test
     * Dimitris Lygizos
     */
    // insert at index -3 (invalid argument error) on list_b
    index = -3;
    anElement = "faulty entry";
    InsertNth(&list_b, index, anElement);

    // insert at index 2 on list_b
    index = 2;
    anElement = "Index2";
    InsertNth(&list_b, index, anElement);
    /* Shall now contain
     * efta
     * 13
     * Index2
     * 28
     * Last_element
     */

    printf("Mylist\n");
    printContents(myList, printfunction);
	printf("%s\n", "========================");
    printf("list_b\n");
	printContents(list_b, printfunction);
	printf("%s\n", "========================");
    
    // Remove node from a list which returns ok
    /* list_b shall now contain
     * efta
     * Index2
     * 28
     * last_element
     */
    x = "13";
    removeNode(&list_b, x, strcmp );

    // remove node from a list where there is no such node (nothing changes)
    anElement = "Invalid node";
    removeNode(&myList, anElement, strcmp );

	// Move node from myList to list_b
    MoveNode( &list_b, &myList );
    /* myList shall now contain
     * third_element
     * Second_element
     * Test
     * Dimitris Lygizos
     *
     * list_b shall now contain
     * forth_element
     * efta
     * Index2
     * 28
     * last_element
     */

    search = "Third_element";	// print 1 times
	printf( "value: %s, occurs %d times in ListA\n", search, Count(myList, search, strcmp));

	// Get from index 13 from myList, Invalid index error doesn't print any node
	index = 13;
	if( (newNode = GetNth(myList, index)) != NULL )
		printf( "value of index: %d\t is %s\n", index, (char *) newNode->data);

	index = 1;		// Get from myList node="Second_element"
	if( (newNode = GetNth(myList, index)) != NULL )
			printf( "value of index: %d\t is %s\n", index, (char *) newNode->data);

	if( (anElement = (char *)Pop(&myList)) != NULL )	// Get and remove TOP from myList "third_element"
		printf("Pop(ListA): %s\n", anElement );
	/* myList shall now contain
     * Second_element
     * Test
     * Dimitris Lygizos
     *
     * list_b shall now contain
     * forth_element
     * efta
     * Index2
     * 28
     * last_element
     */

    Append( &myList, &list_b );
    /* myList shall now contain
     * Second_element
     * Test
     * Dimitris Lygizos
     * forth_element
     * efta
     * Index2
     * 28
     * last_element
     * list_b shall now contains nothing
     */


    if( (newNode = GetNth(list_b, index)) != NULL )		// list_b is empty now print invalid index parameter
    		printf( "value of index: %d\t is %s\n", index, (char *)newNode->data);

    /* Shall print
     * Second_element
     * Test
     * Dimitris Lygizos
     * forth_element
     * efta
     * Index2
     * 28
     * last_element
     */
    printf("Mylist\n");
    printContents( myList, printfunction );
    printf("%s\n", "========================");

    /* Shall return empty list error */
	printf("list_b\n");
    printContents( list_b, printfunction );
    printf("%s\n", "========================");

    MergeSort(&myList, strcmp);		// perform mergesort
    printf("myList after MergeSort\n");
    printContents( myList, printfunction );
	printf("%s\n", "========================");

	newNode = malloc(sizeof(node));	// try sorted insert
	x = "Dimitris Lygizos";
	newNode->data = x;
	SortedInsert( &myList, newNode, strcmp );

	printf("myList after sortedinsert of Dimitris_Lygizos\n");
	printContents( myList, printfunction );
	printf("%s\n", "========================");

	RemoveDuplicatesSorted( myList, strcmp );	// try duplicate removal function
	printf("myList after removal of duplicates\n");
	printContents(myList, printfunction);
	printf("%s\n", "========================");

	front = NULL;
	back = NULL;
	AlternatingSplit( &myList, &front, &back );
	printf("front\n");
	printContents(front, printfunction);
	printf("%s\n", "========================");
	printf("back\n");
	printContents(back, printfunction);
	printf("%s\n", "========================");
	Append(&front, &back);
	Append(&myList, &front);
	printf("myList after append of back and front\n");
	printContents(myList, printfunction);
	printf("%s\n", "========================");

	InsertSort(&list_b, strcmp);	// Request on an empty list
    InsertSort(&myList, strcmp);	// InsertSort of myList

    /* Shall print
     * 28
     * Dimitris Lygizos
     * Index2
     * Last_element
     * Second_element
     * Test
     * efta
     * forth_element
     */
    printf("myList after InsertSort\n");
    printContents(myList, printfunction);
    printf("%s\n", "========================");

    DeleteList(&front);	// clear front,back
	DeleteList(&back);
	front = NULL;
	back = NULL;

    FrontBackSplit( &myList, &front, &back );
    printf("myList after FrontBackSplit\n");	// myList is empty now
    printContents(myList, printfunction);
    printf("%s\n", "========================");
    printf("front pointer after FrontBackSplit\n");
    printContents(front, printfunction);
    printf("%s\n", "========================");
    printf("back pointer after FrontBackSplit\n");
    printContents(back, printfunction);
    printf("%s\n", "========================");
	
    printf("new List from sorted merge of front and back\n");
    printContents( SortedMerge(front, back, strcmp), printfunction);
    printf("%s\n", "========================");

    DeleteList(&myList);	// clear lists
    DeleteList(&list_b);
    DeleteList(&front);
	DeleteList(&back);

    return (EXIT_SUCCESS);
}

