/*
Takes a list and a data value.
Creates a new link with the given data and pushes
it onto the front of the list.
The list is not passed in by its head pointer.
Instead the list is passed in as a "reference" pointer
to the head pointer -- this allows us
to modify the caller's memory.
*/
#include "LinkedList.h"
#include "assert.h"

void Push( node** headRef, void *data ) {
    node* newNode = malloc(sizeof(node));
    newNode->data = data;
    newNode->next = *headRef;
    *headRef = newNode;
    // The '*' to dereferences back to the real head
    // ditto
}

void alt_Push(node **headRef, node* newNode)
{
    newNode->next = *headRef;
    *headRef = newNode;
}

/*
 * find a specific element in the list
 * based on the element argument.
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 */
node* find( node *head, void *element, int (*cmpfunc)(void *, void *) )
{
    node* current;
    
    for(current = head; current; current = current->next) {
        if( !cmpfunc(current->data, element) ) return current;
    }
    
    return NULL;
}

void printContents( node* head, void (*printfunc) () ) {
    node *current = head;
    int count = 0;
    
    if( current == '\0' ) fprintf(stderr, "PrintContents: List is still empty!\n");

    while( current != NULL ) {
        printf("Data #%d: ", ++count);
        printfunc(current->data);
        current = current->next;
    }
}

/*
Given a linked list head pointer, compute
and return the number of nodes in the list.
*/
int Length( node* head ) {
    node* current = head;
    int count = 0;
    while (current != NULL) {
        count++;
        current = current->next;
    }
    return count;
}

/*
 * Given a list and an integer argument, return the number of times
 * that integer occurs in the list.
 */
int Count( node* head, void *searchFor, int (*cmpfunc) (void *, void *) ) {
    node* current = head;
    int count = 0;
    
    while( current != NULL ) {
        if( !cmpfunc(current->data, searchFor) ) count++;
        current = current->next;
    }
    
    return count;
}

/*
 * Free all the list nodes
 */
void DeleteList( node** headRef ) {
    node *current = *headRef;
    node *free_me;
    
    while( current != NULL ) {
    	free_me = current;
        current = current->next;
        free( free_me->data );
        free( free_me );
    }

    *headRef = NULL;
}

/*
 * The opposite of Push(). Takes a non-empty list
 * and removes the front node, and returns the data
 * which was in that node.
 */
void *Pop( node** headRef) {
    node *head = *headRef;

    if( head == '\0' ) {
    	fprintf(stderr, "pop: Request on an empty list!\n");
    	return '\0';
    }
    void *giveaway = head->data;
    *headRef = head->next;
    free(head);
    
    return giveaway;
}

node* alt_Pop( node** headRef ) {

	if( *headRef == NULL )
		return NULL;

    node *head = *headRef;
    *headRef = head->next;
    
    return head;
    
}

/* Given a list and an index, return the node
 * in the nth node of the list.
 * Assert fails if the index is invalid (outside 0..lengh-1).
 */
node *GetNth( node* head, int index ) {
    node* current = head;
    int list_size, count = 0;
    list_size = Length(head);
    if(index < 0 || index > list_size ) fprintf(stderr, "GetNth: Invalid index parameter %d\n", index);

    while( current != NULL ) {
        if( count == index ) break;
        count++;
        current = current->next;
    }
    return current;
}

/*
 * Remove (and only remove) a specific node in the list based on the given argument -element
 * cmpfunc function argument is an argument which:
 * Returns: -1 if second argument is greater than the first
 * 0 if the arguments are equal
 * 1 if the first argument is greater than the second
 */
void removeNode(node **headRef, void *element, int (*cmpfunc) (void *, void *))
{
	node *current, *tmp;

	if(*headRef == NULL) fprintf(stderr, "removeNode: Request on an empty list!\n");
	else {
		current = *headRef;
		if( !cmpfunc((*headRef)->data, element) ) {
			*headRef = current->next;
			free(current);
		} else {
			while(current->next) {
				if( !cmpfunc(current->next->data, element) ) {
					tmp = current->next;
					current->next = tmp->next;
					free(tmp);
					break;
				}
				current = current->next;
			}
		}
	}
}

/*
A more general version of Push().
Given a list, an index 'n' in the range 0..length,
and a data element, add a new node to the list so
that it has the given index.
*/
void InsertNth( node** headRef, int index, void *data ) {
    node *current = *headRef;
    node *prev_node = current;
    int count = 0;
    
    if(index < -1 || index > Length(*headRef) ) {
    	fprintf(stderr, "InsertNth: Invalid index parameter %d\n", index);
    } else {
		node *newNode = (node*) malloc( sizeof(node) );
		newNode->data = data;

		if(index == 0) {
			newNode->next = *headRef;
			*headRef = newNode;
		} else {
			while( current != NULL ) {
				count++;
				current = current->next;
				if( count == index ) {
					prev_node->next = newNode;
					newNode->next = current;
				}
				prev_node = current;
			}
		}
    }
}

/*
 * given a list that is sorted in increasing order and
 * a single node, inserts this node into the correct sorted position in the list.
 * SortedInsert() takes an existing node, and just rearranges pointers
 * to insert it into the list.
 */
void SortedInsert( node** headRef, node* newNode, int (*cmpfunc)(void *, void *) ) {
    
    node *current;
    
    if(newNode == '\0') return;

    if( *headRef == NULL ) {
    	newNode->next = NULL;
    	*headRef = newNode;
    }
    // if (*headRef)->data >= newNode->data insert newNode at head
    else if( !(cmpfunc( (*headRef)->data, newNode->data ) < 0)  ) {
    	alt_Push( headRef, newNode );
	} else {
    	current = *headRef;
    	while( current->next )
    	{
    		// if the following node in the list has data bigger than or equal to node we want to insert then break
    		// and place new node after the current node
    		if( !(cmpfunc(current->next->data, newNode->data) < 0)  ) { break; }
    		current = current->next;
    	}
    	alt_Push( &(current->next), newNode );
    }
}

// Given a list, change it to be in sorted order (using SortedInsert()).
void InsertSort( node** headRef, int (*cmpfunc) (void *, void *) ) {
    node *current, *list_b = NULL;
    if( *headRef == NULL ) fprintf(stderr, "InsertSort: Request on an empty list!\n");
    
    /*
     * Pop from the UNSORTED list
     * and use the SortedInsert() function
     */
    while( (current = alt_Pop( headRef )) != NULL )
	{
		SortedInsert( &list_b, current, cmpfunc );
	}
    *headRef = list_b;
}

// Append 'b' onto the end of 'a', and then set 'b' to NULL.
void Append( node** aRef,  node** bRef ) {
    node *current = *aRef;

    if( *aRef == NULL ) {
        *aRef = *bRef;
    } else {
        while(current->next != NULL) {
            current = current->next;
        }
        current->next = *bRef;
    }
    *bRef = NULL;
}

/*
 * Split the nodes of the given list into front and back halves,
 * and return the two lists using the reference parameters.
 * If the length is odd, the extra node should go in the front list.
 * WARNING: This function DOES NOT preserve the source list!!!
*/
void FrontBackSplit( node** source,
 node** frontRef,  node** backRef) {
    
    node *fast_pointer = *source;
    node *slow_pointer = *source;
    node* delay;
    int count=0;
    
    *frontRef = *source;
    while( fast_pointer != NULL ) {
    	/*
    	 * slow_pointer advances
    	 * the half speed fast_pointer does
    	 */
        if( count%2 == 0 ) {
                delay = slow_pointer;
                slow_pointer = slow_pointer->next;
        }
        count++;
        fast_pointer = fast_pointer->next;
    }
    *backRef = slow_pointer;
    delay->next = NULL;	/* front list stops here */
    *source = NULL; /* eliminate the source list */
}

/*
 * Given the source list, split its nodes into two shorter lists.
 * If we number the elements 0, 1, 2, ... then all the even elements
 * should go in the first list, and all the odd elements in the second.
 * The elements in the new lists may be in any order.
 * WARNING: This function DOES NOT preserve the source list!!!
*/
void AlternatingSplit( node** source,
	node** aRef,  node** bRef ) {
	int i=Length(*source);

	for(; i>0; i--)
	{
		if(i%2 == 0)
			MoveNode(aRef, source);
		else
			MoveNode(bRef, source);
	}
	*source = NULL;
}

/*
 * Remove duplicates from a list
 */
void RemoveDuplicates( node* head, int (*cmpfunc) (void *, void *) ) {
    node *current = head;
    node *temp_list = NULL;
    node *prevNode;
    
    while( current )
    {
        if( find(temp_list, current->data, cmpfunc) != NULL ) {
        	prevNode->next = current->next;
        	free(current);
            current = prevNode->next;
        } else {
        	Push( &temp_list, current->data );
        	prevNode = current;
        	current = current->next;
        }
    }
}

/*
 * Remove any duplicates from a sorted list.
 * Note: Single traversal efficiency
 */
void RemoveDuplicatesSorted( node *head, int (*cmpfunc) (void *, void *) )
{
	node *rm;

    if( head == '\0' ) { fprintf(stderr, "Remove duplicates of a sorted list: Request on an empty list!\n"); return; }

	while( head->next )
	{
		if( !cmpfunc(head->data, head->next->data) )
		{
			rm = head->next;
			head->next = rm->next;
			free(rm);
		} else
			head = head->next;
	}
}

/*
 * Take the node from the TOP of the source,
 * and move it to the TOP of the dest.
 * It is an error to call this with the source list empty.
 */
void MoveNode( node** destRef,  node** sourceRef ) {
    node* nodeToMove = NULL;
    if( *sourceRef == NULL ) { fprintf(stderr, "MoveNode: Request on an empty source list!\n"); return; }
    
    nodeToMove = alt_Pop( sourceRef ); 	// pop from source
    
    alt_Push( destRef, nodeToMove );	// Push to dest
}

/* helper recursive function for ShuffleMerge() */
void ShuffleMergeHelper( node** headRef, node** a, node** b, int count )
{
    node* nodeToMove;
     
    if( count % 2 == 0 && Length(*a) > 0 ) {
        // pare komvous apo lista a
        nodeToMove = alt_Pop(a);
        ShuffleMergeHelper(headRef, a, b, ++count);
    } else if( count % 2 != 0 && Length(*b) > 0 ) {
        // pare komvous apo lista b
        nodeToMove = alt_Pop(b);
        ShuffleMergeHelper(headRef, a, b, ++count);
    } else if( Length(*a) > 0 ) {
    	*headRef = *a;
    	*a = NULL;
    	return;
    } else if( Length(*b) > 0 ) {
    	*headRef = *b;
    	*b = NULL;
    	return;
    }
    else {
        return;
    }

    alt_Push( headRef, nodeToMove );
}

/*
 * Merge the nodes of the two lists into a single list taking a node
 * alternately from each list, and return the new list. [recursion]
 * WARNING: This function DOES NOT preserve the source lists!!!
*/
node* ShuffleMerge(node** a,  node** b) {
    node *head = NULL;
    if( *a == '\0' && *b == '\0' ) fprintf(stderr, "Shuffle Merge: list a AND list b are empty!\n");
    else ShuffleMergeHelper(&head, a, b, 0);
    return head;
}

/*
Takes two lists sorted in increasing order, and
splices their nodes together to make one big
sorted list which is returned.
WARNING: This function DOES NOT preserve the source lists!!!
*/
node* SortedMerge(node* a, node* b, int (*cmpfunc)(void *, void *)) {

	node *newList = NULL;

	while(a)
	{
		while(b)
		{
			// if b->data < a->data cmpfunc(b->data, a->data) == -1
			if( cmpfunc(b->data, a->data) < 0 )
				MoveNode(&newList, &b);
			else
				break;
		}
		MoveNode(&newList, &a);
	}
	while(b) MoveNode(&newList, &b);	// Clean-up B's elements!
	//Reverse(&newList);
    RecursiveReverse(&newList);			// Self - explained: Reverse the elements of the list!
	return newList;
}

/*
 * NOTE: Recursion!
 * Split the list into two smaller lists, recursively sort those lists,
 * and finally merge the two sorted lists together into a single sorted list.
 */
void MergeSort(node **headRef, int (*cmpfunc) (void *, void *) )
{
	node *f, *b;
	if( *headRef == '\0' ) return;			// Empty List case

	FrontBackSplit(headRef, &f, &b);		// Split into two smaller sublists

	if( f->next != '\0' ) MergeSort(&f, cmpfunc);	// while f not empty recurse
	if( b->next != '\0' ) MergeSort(&b, cmpfunc);	// while b not empty recurse

	*headRef = SortedMerge(f, b, cmpfunc);			// sort f, b and return fix head pointer
}

/*
 * Reverse the given linked list by changing its .next pointers
 * and its head pointer. Takes a pointer (reference) to the head pointer.
 */
void Reverse(node **headRef)
{
	node *newList = '\0';

	if( *headRef == NULL ) return;		// Empty list case

	while( *headRef ) {					// while elements on list, push them on a new list
		MoveNode(&newList, headRef);
	}
	*headRef = newList;					// fix head pointer
}

/*
 * Recursively reverses the given linked list by changing its .next pointers
 * and its head pointer in one pass of the list.
 */
void RecursiveReverse(node **headRef)
{
	node *first, *rest;

	if( *headRef == '\0' ) return;		// Empty list case

	first = *headRef;
	rest = first->next;

	if( rest == '\0' ) return;			// empty rest base case

	/*
	 * Recursively reverse the smaller {2, 3} case
	 * after: rest = {3, 2}
	 */
	RecursiveReverse(&rest);

	first->next->next = first;
	first->next = NULL;

	*headRef = rest;		// fix the head pointer
}

/*
 * Recursively reverses the given linked list by changing its .next pointers
 * and its head pointer in one pass of the list. 2 arguments way
 */
void RecursiveReverse2(node **headRef, node* cur)
{
	if( *headRef == NULL ) return;		// Empty list case
	if( cur->next != '\0' ) {
		RecursiveReverse2(headRef, cur->next);
		cur->next->next = cur;
		cur->next = '\0';
	} else {
		*headRef = cur;
	}
}



void printReversedList( node* head, int count, void (*printfunc)(void *) )
{
    if(head->next != NULL) {
        printReversedList( head->next, --count, printfunc );
    } else {
        count--;
    }
    printf("Data #%d = ", count);
    printfunc(head->data);
}
